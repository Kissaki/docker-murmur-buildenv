FROM alpine:latest
RUN apk update && apk add git qt5-qtbase-dev g++ protobuf-dev make libcap-dev boost-dev avahi-dev
ENV PATH="/usr/lib/qt5/bin:${PATH}"
